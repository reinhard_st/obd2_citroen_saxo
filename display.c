/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel                                    */
/*     infos von:                                                         */
/*                                                                        */
/*    file: display.c                                                     */
/*          anzeige relevanter daten beim fahren                          */
/*                                                                        */
/**************************************************************************/

// Get and set terminal attributes
#include <unistd.h>
#include <linux/termios.h>
#include <linux/serial.h>

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define OBD_DISPLAY
#include "display.h"
#include "init.h"

void display(void) 
{
   int delay=200000;
	int fd, flags, fd_stdin=1; 
	int k = 0, e=0, first=1;
	struct timeval tv, tv_alt;
	int c=0, i, m1[14],m2[14],m3[14],m4[14], ibat, proz_alt=255;
	float ladung=0, strecke=0, v, sec, ssec=0, ladung_alt=0, ahps=0.77;
	float strecke_alt=0, kmpah=0;

// standart-input auf zeichenweise und timeout umstellen:
	fd_stdin = STDIN_FILENO;
	tcgetattr(fd_stdin, &oldterm_stdin);
	tcgetattr(fd_stdin, &term_stdin);
	term_stdin.c_lflag = term_stdin.c_lflag & (!ICANON); // nicht zeilenweise
	term_stdin.c_cc[VMIN] = 0;	// mindestens so viel zeichen
	term_stdin.c_cc[VTIME] = 1; // time out in 1/10s
	tcsetattr(fd_stdin, TCSANOW, &term_stdin);

	for (i=0;i<13;i++)
	{
		m1[i]=0;
		m2[i]=0;
		m3[i]=0;
		m4[i]=0;
	}

	clrscr();
	printf("\033[3;2f");
	delay = 15000;
 
	if (ecm_init(&fd) == 0)
	{
		clrscr();
		gettimeofday(&tv_alt, NULL);
		while (k!='q')
		{
			e = 0;
			k = getchar();
//  messwerte 1 abfragen:
			e = e + measure(fd, 1, m1);
			usleep(delay);
//  messwerte 2 abfragen:
			e = e + measure(fd, 2, m2);
			usleep(delay);
//  messwerte 3 abfragen:
			e = e + measure(fd, 3, m3);
			usleep(delay);
//  messwerte 4 abfragen:
			e = e + measure(fd, 4, m4);
			usleep(delay);
			if (e == 0)
			{
				gettimeofday(&tv, NULL);
				sec=tv.tv_sec-tv_alt.tv_sec + (tv.tv_usec-tv_alt.tv_usec)/1000000.0;
				tv_alt.tv_sec = tv.tv_sec;
				tv_alt.tv_usec = tv.tv_usec;
				ssec = ssec + sec;
// batt-strom:
				if ((char)m2[2] > 0)	
					ibat=(char)m2[2]*2.23;
				else
					ibat=(char)m2[2]*1.47;

// messwerte berechnen:
				ladung = ladung + ibat*sec/3600.0;  
				v = m1[0]*0.4365; // experimentell ermittelt
				strecke = strecke + v*sec/3.6;
//ahps:		
				if (first)  // 1. mal
				{
					first = 0;
					proz_alt = m3[6];
					ladung_alt = ladung;
					strecke_alt = strecke;
				}
				else if ((proz_alt - m3[6])>=5)
				{
					ahps = (ladung-ladung_alt)/((proz_alt-m3[6])*100.0/255.0);
					proz_alt = m3[6];
					kmpah = ((strecke-strecke_alt)/(ladung-ladung_alt));
					ladung_alt = ladung;
					strecke_alt = strecke;
				}
// anzeigen:
				printf("\033[1;2f Batterie:\n");
				printf(" %3dV  %+4dA  %2d%%  %4.2fAh\n\n", m2[0], ibat, (m3[6]*100)/255, ladung);
				printf(" Tacho\n");
				printf(" %3.0fkm/h  %5.2fkm  G%4.2fV  %2d:%02d    \n\n", v, strecke/1000, m1[2]*5.0/256, (int)(ssec/60), (int)(ssec)%60);
				printf(" %4.2fkm/%%  Mittel: %4.2fkm/Ah  %4.2fAh/%%   \n\n", (v/ibat)*ahps, kmpah/1000, ahps);
				printf(" Diverses\n");
				printf("B:%+2d°C  E:%+2d°C  %+4dA        ", m3[4]-0x40, ((0xe5-m3[2])* 2)/3, (int)((char)m1[1]*1.7)); 
		
// warten
				usleep(delay);
			}
			else if (e>2)  // fehler=> neu initialisieren
			{
				ecm_exit(fd);
				usleep(delay);
				ecm_init(&fd);
				clrscr();
				printf("\033[3;2f");
			}
			else
			{
				clrscr();
				printf("\033[3;2f");
			}
		}
		ecm_exit(fd);
	}	
	tcsetattr(fd_stdin, TCSANOW, &oldterm_stdin); 
//  term_stdin.c_lflag = term_stdin.c_lflag & ~(ICANON|ECHO); 
//  term_stdin.c_cc[VMIN] = 1;	// mindestens so viel zeichen
//  term_stdin.c_cc[VTIME] = 0; // time out in 1/10s
//  tcsetattr(fd_stdin, TCSANOW, &term_stdin);

	printf ("\nZurück zum Hauptmenü mit enter-Taste!\n");
	getchar();

}

