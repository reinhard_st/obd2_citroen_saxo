/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel (ft232)                            */
/*     file: display.h                                                    */
/*                 anzeige relevanter werte bei der fahrt                 */
/*                                                                        */
/*                                                                        */
/**************************************************************************/

#ifdef OBD_DISPLAY
	#define EXT extern
#else
	#define EXT 
#endif 

#include <linux/termios.h>
EXT	struct termios term_stdin, oldterm_stdin; //geht local nicht?

EXT void display(void);

#undef EXT
