/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel                                    */
/*     infos von:                                                         */
/*                                                                        */
/*     23.12.2014: ecm_init und send_bit                                  */
/*     24.12.2014: 1. test, 106er antowrtet, siehe init_1.txt             */
/*     26.12.2014: initialisierung und messwerte abfragen (spannung)      */
/*     31.12.2014: nur mehr wichtige daten werden angezeit, mit steuerung */
/*                 auf bestimmte positionen im bildschirm                 */
/*                                                                        */
/**************************************************************************/


// Get and set terminal attributes
#include <linux/termios.h>
#include <linux/serial.h>
 
// Control time (not like in Back to the Future)
#include <time.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#define STOP  0
#define START 1

   int delay=200000;
	int fd, flags; 

void send_bit(int bit)
{
	if (bit)
		ioctl(fd, TIOCCBRK);	// löscht break => leitung high
	else 
		ioctl(fd, TIOCSBRK);	// sendet break => leitung low


}


void ecm_init()
{ 
	int p, i, c=0;
   // Engine ECM address peugeot 106e
   int address=0x71; 

   // clear DTR and RTS
   ioctl (fd, TIOCMGET, &flags);
   flags &= ~(TIOCM_DTR | TIOCM_RTS);
   ioctl (fd, TIOCMSET, &flags);
 
   // 200000 µs delay = 200ms
   usleep (delay);
    
   // External function: Set start bit
   send_bit (0);     
   usleep (delay);
 
   // Set parity = 1 because of even parity
   p = 1;
   for (i=0; i<8; i++) {  
       
      // Bits: LSB first
      // Shifting bits right with AND logic
      int bit = (address >> i) & 0x1;
      send_bit (bit);

 
      // Check parity with XOR
      //p = p ^ bit;
      usleep (delay);
   }
   // Odd parity
   //send_bit (0);     
   //usleep (delay);
 
   // Set stop bit
   send_bit (1);    
   usleep (delay);
 
   // Set DTR
   ioctl (fd, TIOCMGET, &flags);
   flags |= TIOCM_DTR;
   ioctl (fd, TIOCMSET, &flags);

// einige bytes empfangen, bis 0x33 kommt,
	do
	{
		if (read(fd,&c,1)>0)   
		{     
 			printf("%X ",c);
		}
	}
	while (c != 0x33);
//  dann antworten:
	c = 0x83; write (fd, &c,1);
	c = 0xF0; write (fd, &c,1);
	c = 0x05; write (fd, &c,1);
	c = 0x87; write (fd, &c,1);  // checksumme;
// und nochmals auf antwort warten:
	do
	{
		if (read(fd,&c,1)>0)   
		{     
 			printf("%X ",c);
		}
	}
	while (c != 0x47);
// noch kurz warten
	usleep (delay);
// fertig!

}

void clrscr(void)
{
	int i;
		printf("\033[1;1f                              \n");
		for (i=0; i<10; i++)
			printf("                                   \n");
}	

int main() 
{

  struct termios term_stdin, oldterm_stdin;
  int fd_stdin; // standart-eingabe
  int k = 0, k1=0;
    
   struct termios oldtio, newtio;
   struct serial_struct new_serdrvinfo; 
	//struct timespec tp;
	struct timeval tv, tv_alt;
	int c=0, i, m[14],m1[14],m2[14],m3[14],m4[14], ibat, proz_alt=255;
	float ladung=0, strecke=0, v, sec, ssec=0, ladung_alt=0, ahps=0;

// standart-input auf zeichenweise und timeout umstellen:
	fd_stdin = fileno(stdin);
  tcgetattr(fd_stdin, &oldterm_stdin);
  memcpy(&term_stdin, &oldterm_stdin, sizeof(oldterm_stdin));
  term_stdin.c_lflag = term_stdin.c_lflag & (!ICANON); // nicht zeilenweise
  term_stdin.c_cc[VMIN] = 0;	// mindestens so viel zeichen
  term_stdin.c_cc[VTIME] = 1; // time out in 1/10s
  tcsetattr(fd_stdin, TCSANOW, &term_stdin);

	for (i=0;i<13;i++)
	{
		m[i]=0;
		m1[i]=0;
		m2[i]=0;
		m3[i]=0;
		m4[i]=0;
	}

   // USB -> KKL converter is mounted as /dev/ttyUSB0
   fd = open ("/dev/ttyUSB0", O_SYNC | O_RDWR | O_NOCTTY);
 	if (fd<0)
	{
		printf("\033[31merror opening /dev/ttyUSB0\033[0m\n");
		exit (1);
	}
   tcgetattr (fd, &oldtio);
   tcgetattr (fd, &newtio);
   newtio.c_cflag = B38400 | CS8 | CLOCAL | CREAD; // nur bei 38400 kann beliebige baudrate eingestellt werden.
   newtio.c_iflag =  IGNPAR ;
   newtio.c_oflag = 0;
//   newtio.c_cc[VMIN] = 1;
//   newtio.c_cc[VTIME] = 0;
   newtio.c_cc[VMIN] = 0;	// warte auf min 0 zeichen
   newtio.c_cc[VTIME] = 5;	// timeout 1/10 sec
 
   tcflush (fd, TCIFLUSH);
   tcsetattr (fd, TCSANOW, &newtio);



  // Add
  // Change serial driver settings: baudrate auf 10400
  ioctl(fd, TIOCGSERIAL, &new_serdrvinfo); // read driver settings
  new_serdrvinfo.flags |= ASYNC_SPD_CUST;  // activate custom baud rate
  new_serdrvinfo.custom_divisor = 2307;  // set custom divisor (ft232)
  ioctl(fd, TIOCSSERIAL, &new_serdrvinfo); // write new driver settings


//	printf ("ft232 detected\n");
//	printf ("initialising .....\n");
	ecm_init();
//	printf("\n *** verbindung hergestellt! ***\n");

clrscr();
delay = 5000;
//clock_gettime(CLOCK_REALTIME_COARSE, &tp);
gettimeofday(&tv_alt, NULL);
 
	while (k!='q')
	{
		k = getchar();
//  messwerte 1 abfragen:
		c = 0x83; write (fd, &c,1);	
		c = 0x11; write (fd, &c,1);
		c = 0x1; write (fd, &c,1);
		c = 0x6A; write (fd, &c,1);  // checksumme;
// auf antwort warten: diese 4 byte und 3 antwortbyte einlesen
		for (i=0;i<7;i++)
		{
			if (read(fd,&c,1)<1)
			{
				printf("\nerror, messwerte 1 abfrage\n");k1='e';
			}   
		}
// 12 messwerte und checksumme einlesen:
		for (i=0; i<13; i++)
		{
			if (read(fd,&(m[i]),1)>0)
			{
					m1[i]=m[i]; k1=0;
			}	
			else
			{
				printf("\nerror, messwerte 1 einlesen: %d\n",i);k1='e';
			}   

		}
// messwerte anzeigen:
//		printf("v:%2dkm/h I:%2dA G:%3.1fV \n",m1[0], (char)m1[1], m1[2]*5.0/256);
// warten
		usleep(delay);
//  messwerte 2 abfragen:
		c = 0x83; write (fd, &c,1);	
		c = 0x11; write (fd, &c,1);
		c = 0x2; write (fd, &c,1);
		c = 0x69; write (fd, &c,1);  // checksumme;
// auf antwort warten: diese 4 byte und 3 antwortbyte einlesen
		for (i=0;i<7;i++)
		{
			if (read(fd,&c,1)<1)
			{
				printf("\nerror, messwerte 2 abfrage\n");k1='e';
			}   
		}
 
// 12 messwerte und checksumme einlesen:
		for (i=0; i<13; i++)
		{
			if (read(fd,&(m[i]),1)>0)        
			{
					m2[i]=m[i]; k1=0;
			}	
			else
			{
				printf("\nerror, messwerte 2 einlesen: %d\n",i);k1='e';
			}   
		}
// messwerte anzeigen:
//		printf("Ubat: %dV  12V:%4.2f, Ibat:%d\n",m2[0],m2[1]*20.0/256,(char)m2[2]*2);
		
// warten
		usleep(delay);
//  messwerte 3 abfragen:
		c = 0x83; write (fd, &c,1);	
		c = 0x11; write (fd, &c,1);
		c = 0x3; write (fd, &c,1);
		c = 0x68; write (fd, &c,1);  // checksumme;
// auf antwort warten: diese 4 byte und 3 antwortbyte einlesen
		for (i=0;i<7;i++)
		{
			if (read(fd,&c,1)<1)
			{
				printf("\nerror, messwerte 3 abfrage\n");k1='e';
			}   
		}
// 12 messwerte und checksumme einlesen:
		for (i=0; i<13; i++)
		{
			if (read(fd,&(m[i]),1)>0)        
			{
					m3[i]=m[i]; k1=0;
			}	
			else
			{
				printf("\nerror, messwerte 3 einlesen: %d\n",i);k1='e';
			}   
		}
// messwerte anzeigen:
//		printf("Tbat:%2d°C Ladung:%2d%%\n", m3[4]-0x40, (m3[6]*100)/255);
// warten
		usleep(delay);

//  messwerte 4 abfragen:
		c = 0x83; write (fd, &c,1);	
		c = 0x11; write (fd, &c,1);
		c = 0x4; write (fd, &c,1);
		c = 0x67; write (fd, &c,1);  // checksumme;
// auf antwort warten: diese 4 byte und 3 antwortbyte einlesen
		for (i=0;i<7;i++)
		{
			if (read(fd,&c,1)<1)
			{
				printf("\nerror, messwerte 4 abfrage\n");k1='e';
			}   
		}
 
// 12 messwerte und checksumme einlesen:
		for (i=0; i<13; i++)
		{
			if (read(fd,&(m[i]),1)>0)        
			{
					m4[i]=m[i]; k1=0;
			}	
			else
			{
				printf("\nerror, messwerte 4 einlesen: %d\n",i);k1='e';
			}   
		}
		if (k1!='e')
		{
gettimeofday(&tv, NULL);
sec=tv.tv_sec-tv_alt.tv_sec + (tv.tv_usec-tv_alt.tv_usec)/1000000.0;
tv_alt.tv_sec = tv.tv_sec;
tv_alt.tv_usec = tv.tv_usec;
ssec = ssec + sec;
//printf ("  s:%7.2f\n",ssec);
// batt-strom:
		if ((char)m2[2] > 0)	
			ibat=(char)m2[2]*2.23;
		else
			ibat=(char)m2[2]*1.47;
//ahps:
		if ((proz_alt - m3[6])>=5)
		{
			ahps = (ladung-ladung_alt)/((proz_alt-m3[6])*100.0/255.0);
			proz_alt = m3[6];
			ladung_alt = ladung;
		}
// messwerte anzeigen:
		ladung = ladung + ibat*sec/3600.0;  
		v = m1[0]*0.45;
		strecke = strecke + v*sec/3.6;
		printf("\033[1;2f Batterie:\n");
		printf(" %3dV   %+4dA  %2d%%  %4.2fAh\n\n", m2[0], ibat, (m3[6]*100)/255, ladung);
		printf(" Tacho\n");
		printf(" %3.0fkm/h  %5.2fkm    G%4.2fV  %7.2fsec\n\n", v, strecke/1000, m1[2]*5.0/256, sec);
		printf(" Diverses\n");
		printf("B:%+2d°C  E:%+2d°C  %+4dA  %5.3f", m3[4]-0x40, ((0xe5-m3[2])* 2)/3, (int)((char)m1[1]*1.7), ahps); 
		}
// warten
		usleep(delay);


	}

// kommunikation beenden:
	c = 0x82; write (fd, &c,1);	
	c = 0xf; write (fd, &c,1);	
	c = 0x6e; write (fd, &c,1);	

	close (fd);
	tcsetattr(fd_stdin, TCSANOW, &oldterm_stdin); 
//  term_stdin.c_lflag = term_stdin.c_lflag | ICANON; // nicht zeilenweise
//  term_stdin.c_cc[VMIN] = 1;	// mindestens so viel zeichen
//  term_stdin.c_cc[VTIME] = 0; // time out in 1/10s
//  tcsetattr(fd_stdin, TCSANOW, &term_stdin);

   return 0;
}
