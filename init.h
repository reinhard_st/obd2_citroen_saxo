/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel (ft232)                            */
/*     file: init.h                                                      */
/*           initialisierung der schnittstelle /dev/ttyUSB0               */
/*                       und des steuergerätes                            */
/*                                                                        */
/**************************************************************************/

#ifdef OBD_INIT
	#define EXT extern
#else
	#define EXT 
#endif 

#include <linux/termios.h>
#include <linux/serial.h>

// geht local nicht!?
EXT   struct termios oldtio, newtio;
EXT   struct serial_struct new_serdrvinfo;

EXT int ecm_init(int *fd_ser);
// initialisiert die serielle schnittstelle /dev/ttyUSB0 und über diese
// das steuergerät des peugeot 106e
// in fd_ser wird der fil-descriptor der seriellen schnittstelle zurückgegeben
// rückgabewert: 0..fehlerfrei, !=0..nicht initialisiert

EXT void ecm_exit(int fd); 
// sendet code zum abmelden an steuergerät und schliesst serielle schnittstelle fd

EXT int measure(int fd, int group, int *m);
// fragt das steuergerät über schnittstelle fd um die messwerte der gruppe group
// speichert ergebniss ins feld m, muss 13 elemente haben
// rückgabewert: 0..fehlerfrei, !=0..fehler

EXT int read_error(int fd, int group, int *m);
// wie measure, group muss 0 oder 1 sein.

EXT void clrscr(void);
// löscht den Bildschirm

#undef EXT 
