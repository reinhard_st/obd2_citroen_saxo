/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel (ft232)                            */
/*     file: main.c                                                       */
/*           menü anzeigen und aufruf der entsprechenden funktionen       */
/*                                                                        */
/*                                                                        */
/**************************************************************************/

#include <stdio.h>
#include "ladung.h"
#include "display.h"
#include "fehler.h"

int main() 
{
	int key=0;
	while (key != 'q')
	{
		clrscr();
		printf("\033[1;1f     OBD2-Abfrage Peugeot 106e\n\n");
		printf("   l.....Abfrage Batterie und gespeicherte Ladung\n\n");
		printf("   c.....Löschen gespeicherte Ladung\n\n");
		printf("   w.....setze Wartungsladung\n\n");
		printf("   x.....Löschen Wasserbedarf\n\n");		
		printf("   f.....Fehlerabfrage\n\n");
		printf("   g.....Fehler löschen\n\n");
		printf("   m.....Messung waehrend der Fahrt\n\n");
		printf("   q.....Programm beenden\n\n");
		key = getchar();getchar();
		switch (key)
		{
			case 'l':
			{
				abfr_ladung();
				break;
			}
			case 'c':
			{
				loes_ladung();
				break;
			}
			case 'w':
			{
				wartungs_ladung();
				break;
			}
			case 'x':
			{
				loesche_wasserbedarf();
				break;
			}
			case 'f':
			{
				abfr_fehler();
				break;
			}
			case 'g':
			{
				loes_fehler();
				break;
			}
			case 'm':
			{
				display();
				break;
			}
			case 'q':
				break;
			default:
			{
				printf("funktion noch nicht implementiert!\n");
				usleep(2000000);
			}
		}	
	}
   return 0;
}
