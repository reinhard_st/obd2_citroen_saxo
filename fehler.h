/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel (ft232)                            */
/*     file: fehler.h                                                     */
/*           funktionen für ladung anzeigen, löschen und programmieren    */
/*                                                                        */
/*                                                                        */
/**************************************************************************/


#ifdef OBD_FEHLER
	#define EXT 
#else
	#define EXT extern
#endif

EXT void abfr_fehler(void);
// öffnet schnittstelle, fragt alle relevanten daten ab und gibt sie aus.
// schliesst schnittstelle wieder



EXT void loes_fehler(void);
// öffnet schnittstelle, Löscht die gespeicherten fehler.
// schliesst schnittstelle wieder


#undef EXT
