/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel (ft232)                            */
/*     file: fehler_codes.h                                               */
/*           enhällt die fehlermeldungen zu den codes                     */
/*                                                                        */
/*                                                                        */
/**************************************************************************/


#ifdef OBD_FEHLER
	#define EXT 
#else
	error: you are not allowed to include this file!
#endif

void print_fehlertext(char *fe[],int *m);

EXT char *fe_cd[] =    	
// 12 bytes, je 8 bit => 96 fehlercodes, 1. byte: 0 bis 7, 2.byte 8 bis 15...
// 96 fehlercodes => 96 fehler-beschreibungen
{
// 1.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 2.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 3.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 4.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 5.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"Wassermangel Antriebsbatterie",
"unbekannt",
"unbekannt",
// 6.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 7.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 8.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 9.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 10.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 11.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 12.byte
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt"
};

EXT char *sp_fe_cd[] =    	
// 12 bytes, je 8 bit => 96 fehlercodes, 1. byte: 0 bis 7, 2.byte 8 bis 15...
// 96 fehlercodes => 96 fehler-beschreibungen
{
// 1.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 2.byte sporadisch
"unbekannt",
"unbekannt",
"Antriebsstrom-Zerhacker",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 3.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 4.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 5.byte sporadisch
"Isolationsfehler",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt"
"unbekannt",
"unbekannt",
// 6.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 7.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 8.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 9.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 10.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 11.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
// 12.byte sporadisch
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt",
"unbekannt"
};

#undef EXT
