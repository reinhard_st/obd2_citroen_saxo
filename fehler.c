/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel (ft232)                            */
/*     file: fehler.c                                                     */
/*           funktionen für fehlerspecher, auslesen und löschen           */
/*                                                                        */
/*                                                                        */
/**************************************************************************/

//#include <stdlib.h>
#include <stdio.h>

#include "init.h"

#define OBD_FEHLER
#include "fehler.h"
#include "fehler_codes.h"

void abfr_fehler(void)
{
	int fd, m[14], i, e;
	for (i=0;i<13;i++)
	{
		m[i]=0;
	}
	clrscr();
	printf("\033[1;1f     gespeicherte Fehler:\n\n");
	if (ecm_init(&fd) == 0)
	{
		if (read_error(fd,0,m) == 0)
		{
			printf(" Fehler:\n");
			e=0;
			for (i=0;i<12;i++)
				e = e + m[i];
			if (e)
			{
				for (i=0;i<12;i++)
					printf(" %X",m[i]);
				printf ("\n");
				print_fehlertext(fe_cd,m);
			}
			else 
				printf("  Keine Fehler!\n\n");	
		}
		if (read_error(fd,1,m) == 0)
		{
			printf(" sporadische Fehler:\n");
			e=0;
			for (i=0;i<12;i++)
				e = e + m[i];
			if (e)
			{
				for (i=0;i<12;i++)
					printf(" %X",m[i]);
				printf ("\n");
				print_fehlertext(sp_fe_cd,m);
			}
			else 
				printf("  Keine Fehler!");	
		}
		ecm_exit(fd);
	}

	printf ("\nZurück zum Hauptmenü mit enter-Taste!\n");
	getchar();
}


void loes_fehler(void)
{
//fehler löschen (geht erst nach fehlerabfrage?!)
//pc: 83 F0 1 8B  befehl löschen
//auto: C3 F0 1 4B 
//pc: 83 C3 14 A5  fehler sind zu löschen
//auto: C2 C3 7A  ???

	int fd, i, e=1;
	char c;
	clrscr();
	printf("\033[2;2f Alle Fehler werden gelöscht! bestätigen mit j:");
	c = getchar(); getchar();
	if (c=='j')
	{
		clrscr();
		printf("\033[1;1f   Löschen gespeicherte Fehler:\n\n");
		if (ecm_init(&fd) == 0)
		{
			e=0;
		//  befehl speicherung:
			c = 0x83; write (fd, &c,1);	
			c = 0xF0; write (fd, &c,1);
			c = 1; write (fd, &c,1);
			c = 0x8b; write (fd, &c,1);  // checksumme;
		// auf antwort warten: diese 4 byte und 4 antwortbyte:
			for (i=0;i<8;i++)
			{
				if (read(fd,&c,1)<1)
				{
					printf("\ntime out error, Fehler löschen\n");
					e = 1;
					break;
				}   
			}
			if (e==0)
			{
			//  befehl fehler löschen:
			c = 0x83; write (fd, &c,1);	
			c = 0xc3; write (fd, &c,1);	
			c = 0x14; write (fd, &c,1);
			c = 0xa5; write (fd, &c,1);  // checksumme;
			// auf antwort warten: diese 4 byte und 3 antwortbyte:
				for (i=0;i<7;i++)
				{
					if (read(fd,&c,1)<1)
					{
						printf("\ntime out error, Fehler löschen\n");
						e = 1;
						break;
					}   
				}
			}
			ecm_exit(fd);
		}
		if (e)
			printf("\n\n\033[31m   Fehler beim Fehler-löschen!\033[0m\n\n");
		else
		printf("\n\n\033[32m   Fehler erfolgreich gelöscht!\033[0m\n\n");	
	}
	printf ("\nZurück zum Hauptmenü mit enter-Taste!\n");
	getchar();
}

void print_fehlertext(char *fe[],int *m)
// zeigt die entsprechenden fehlertexte an
{
	int i, j;
	for (i=0; i<12; i++)
	{
		for (j=0; j<8; j++)
		{
			if ( ((m[i]>>j) & 0x01) )
				printf ("%s \n", fe[i*8+j]);
		}
		printf ("\n\n");
	}
}

#undef OBD_LADUNG
