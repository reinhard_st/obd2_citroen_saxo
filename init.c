/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel (ft232)                            */
/*     file: init.c                                                       */
/*           initialisierung der schnittstelle /dev/ttyUSB0               */
/*                       und des steuergerätes                            */
/*                                                                        */
/**************************************************************************/


// Get and set terminal attributes
#include <linux/termios.h>
#include <linux/serial.h>
//#include <term.h>
//#include <curses.h>
 
//#include <time.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#define OBD_INIT 
#include "init.h"

#define STOP  0
#define START 1

void send_bit(int fd, int bit)
{
	if (bit)
		ioctl(fd, TIOCCBRK);	// löscht break => leitung high
	else 
		ioctl(fd, TIOCSBRK);	// sendet break => leitung low


}


int ecm_init(int *fd_ser)
{ 

	int i, c=0;
   int delay=200000;
	int fd, flags; 
   // Engine ECM address peugeot 106e
   int address=0x71; 

//initialisierung der seriellen schnittstelle:
   // USB -> KKL converter is mounted as /dev/ttyUSB0
	printf ("initialisierung......\n");
   fd = open ("/dev/ttyUSB0", O_SYNC | O_RDWR | O_NOCTTY);
 	if (fd<0)
	{
		printf("\033[31merror opening /dev/ttyUSB0\033[0m\n");
		return (1);
	}
	else
		*fd_ser = fd;
   tcgetattr (fd, &oldtio);
   tcgetattr (fd, &newtio);
   newtio.c_cflag = B38400 | CS8 | CLOCAL | CREAD; 
		// nur bei 38400 kann beliebige baudrate eingestellt werden.
   newtio.c_iflag =  IGNPAR ;
   newtio.c_oflag = 0;
	newtio.c_lflag = 0; //kein echo ..... (fuer raspberry)
   newtio.c_cc[VMIN] = 0;	// warte auf min 0 zeichen
   newtio.c_cc[VTIME] = 10;	// timeout 1/10 sec

   tcflush (fd, TCIFLUSH);
   tcsetattr (fd, TCSANOW, &newtio);

	// Change serial driver settings: baudrate auf 10400
	ioctl(fd, TIOCGSERIAL, &new_serdrvinfo); // read driver settings
	new_serdrvinfo.flags |= ASYNC_SPD_CUST;  // activate custom baud rate
	new_serdrvinfo.custom_divisor = 2307;  // set custom divisor (ft232)
	ioctl(fd, TIOCSSERIAL, &new_serdrvinfo); // write new driver settings

// initialisierung des steuergerätes: startbit, adresse, stopbit mit 5 baud
   // clear DTR and RTS
   ioctl (fd, TIOCMGET, &flags);
   flags &= ~(TIOCM_DTR | TIOCM_RTS);
   ioctl (fd, TIOCMSET, &flags);
 
   // 200000 µs delay = 200ms
   usleep (delay);
    
   // External function: Set start bit
   send_bit (fd, 0);     
   usleep (delay);
 
   for (i=0; i<8; i++) {  
       
      // Bits: LSB first
      // Shifting bits right with AND logic
      int bit = (address >> i) & 0x1;
      send_bit (fd, bit);
      usleep (delay);
   }

   // Set stop bit
   send_bit (fd, 1);    
   usleep (delay);
 
   // Set DTR
   ioctl (fd, TIOCMGET, &flags);
   flags |= TIOCM_DTR;
   ioctl (fd, TIOCMSET, &flags);

// einige bytes empfangen, bis 0x33 kommt, mit 10400 baud
	do
	{
		if (read(fd,&c,1)>0)   
		{     
			;
// 			printf("%X ",c);
		}
		else 
		{
			printf("\033[31control unit doesn't respond!\033[0m\n");
			return (2);
		}
	}
	while (c != 0x33);
//  dann antworten:
	c = 0x83; write (fd, &c,1);
	c = 0xF0; write (fd, &c,1);
	c = 0x05; write (fd, &c,1);
	c = 0x87; write (fd, &c,1);  // checksumme;
// und nochmals auf antwort warten:
	do
	{
		if (read(fd,&c,1)>0)   
		{     
			;
// 			printf("%X ",c);
		}
		else
		{
			printf("\033[31control unit doesn't respond!\033[0m\n");
			return (2);
		}
	}
	while (c != 0x47);
	printf("connected to control unit!\n");
// noch kurz warten
	usleep (delay);
// fertig!
	return 0;

}


void ecm_exit(int fd)
{
	char c;
	int i;
	c = 0x82; write (fd, &c,1);	
	c = 0xf; write (fd, &c,1);
	c = 0x6e; write (fd, &c,1);
// auf antwort warten: diese 3 byte und 3 antwortbyte:
	for (i=0;i<6;i++)
	{
		if (read(fd,&c,1)<1)
		{
			printf("\ntime out error, exit-function\n");
		}   
	}
	close (fd);
}



int measure(int fd, int group, int *m)
{
	char c, cin[21];
	int i, j;
	FILE *errorfile;
//  messwerte abfragen:
		c = 0x83; write (fd, &c,1);	
		c = 0x11; write (fd, &c,1);
		c = group; write (fd, &c,1);
		c = 0x6b - group; write (fd, &c,1);  // checksumme;
// auf antwort warten: diese 4 byte und 3 antwortbyte:
		for (i=0;i<7;i++)
		{
			if (read(fd,&(cin[i]),1)<1)
			{
				printf("\ntime out error, messwerte %d abfrage\n", group);
				errorfile = fopen( "errorlog.txt", "a+");
				fprintf (errorfile, "timeout messwert %d:", group);
				for (j=0; j<i; j++)
					fprintf (errorfile, " %x", cin[j]);
				fprintf(errorfile, "\n");
				fclose(errorfile);
				return (1);
			}  
//else printf("%x ",c); 
		}
// 12 messwerte und checksumme einlesen:
		for (i=0;i<13;i++)
		{
			if (read(fd,&(m[i]),1)<1)
			{
				printf("\ntime out error, messwerte %d/%d einlesen\n", group,i);
				return (2);
			}   
		}
		return (0);
}


int read_error(int fd, int group, int *m)	
{
	char c, cin[21];
	int i, j;
	FILE *errorfile;
//  messwerte abfragen:
		c = 0x83; write (fd, &c,1);	
		c = 0x33; write (fd, &c,1);
		c = group; write (fd, &c,1);
		c = 0x49 - group; write (fd, &c,1);  // checksumme;
// auf antwort warten: diese 4 byte und 3 antwortbyte:
		for (i=0;i<7;i++)
		{
			if (read(fd,&(cin[i]),1)<1)
			{
				printf("\ntime out error, fehlerspeicher %d auslesen\n", group);
				errorfile = fopen( "errorlog.txt", "a+");
				fprintf (errorfile, "timeout fehlerspeicher %d:", group);
				for (j=0; j<i; j++)
					fprintf (errorfile, " %x", cin[j]);
				fprintf(errorfile, "\n");
				fclose(errorfile);
				return (1);
			}  
//else printf("%x ",c); 
		}
// 12 messwerte und checksumme einlesen:
		for (i=0;i<13;i++)
		{
			if (read(fd,&(m[i]),1)<1)
			{
				printf("\ntime out error, fehlerauslesen %d/%d einlesen\n", group,i);
				return (2);
			}   
		}
		return (0);
}


/*
void clrscr (void) 
{
  const char *ptr = tigetstr ("clear");
  putp (ptr);
}
*/
void clrscr(void)
{
	int i;
		printf("\033[1;1f                                                             \n");
		for (i=0; i<30; i++)
			printf("                                                            \n");
}	


#undef OBD_INIT
