/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel (ft232)                            */
/*     file: ladung.c                                                     */
/*           funktionen für ladung anzeigen, löschen und programmieren    */
/*                                                                        */
/*                                                                        */
/**************************************************************************/

//#include <stdlib.h>
#include <stdio.h>

#include "init.h"

#define OBD_LADUNG

void abfr_ladung(void)
{
	int fd, m[14], i;
	for (i=0;i<13;i++)
	{
		m[i]=0;
	}
	clrscr();
	printf("\033[1;1f     Batteriewerte und Ladung:\n\n");
	if (ecm_init(&fd) == 0)
	{
	if (measure(fd,2,m) == 0)
	{
		printf("\033[3;1f  Antriebsbat: %3dV     \n", m[0]);
		printf("  12v_Bat: %4.1fV          \n", m[1]*20.0/255.0);
		printf("  Überladung: %3dAh        \n", m[3]*10);
		printf("  Überladung Grenze: %3dAh       \n", m[4]*10);
		printf("  Summe Überladung: %dAh         \n", (m[5]+m[6]*256)*10);
		printf("  Summe Entladung: %dAh          \n", (m[9]+m[10]*256)*10);
		if (measure(fd,3,m) == 0)
		{
			printf("  Temp. Antriebsbat: %d°C         \n", m[4]-0x40);
			printf("  Resterergie: %d%%                \n", m[6]*100/255);
			if (measure(fd,4,m) == 0)
			{
				if (m[7] == 0x70) // noch nicht verifiziert!!
					printf("  Program. Ladung: Initialisierungsladung\n");
				else if (m[7] & 0x01)
					printf("  Program. Ladung: Ausgleichsladung\n");	
				else if (m[7] & 0x02)
					printf("  Program. Ladung: Wartungsladung\n");	
				else 
					printf("  Program. Ladung: keine          \n");	
			}
		}
	}
	ecm_exit(fd);
	}	
	printf ("\nZurück zum Hauptmenü mit enter-Taste!\n");
	getchar();
}


void loes_ladung(void)
{
//annulierung der ladung
//pc: 83 F0 1 8B  befehl löschen 
//auto: C3 F0 1 4B 
//pc: 83 77 82 83  programmierte ladung löschen 
//auto C3 77 82 43 
//pc: 83 F0 5 87 kommunikation initialisieren (wie bei init)
//auto: C3 F0 5 47 
	int fd, i, e=1;
	char c;
	clrscr();
	printf("\033[1;1f   Löschen gespeicherte Ladung:\n\n");
	if (ecm_init(&fd) == 0)
	{
	e=0;
//  befehl speicherung:
	c = 0x83; write (fd, &c,1);	
	c = 0xF0; write (fd, &c,1);
	c = 1; write (fd, &c,1);
	c = 0x8b; write (fd, &c,1);  // checksumme;
// auf antwort warten: diese 4 byte und 4 antwortbyte:
	for (i=0;i<8;i++)
	{
		if (read(fd,&c,1)<1)
		{
			printf("\ntime out error, gespeicherte Ladung löschen\n");
			e = 1;
			break;
		}   
	}
	if (e==0)
	{
	//  befehl programmierte ladung löschen:
		c = 0x83; write (fd, &c,1);	
		c = 0x77; write (fd, &c,1);	
		c = 0x82; write (fd, &c,1);
		c = 0x83; write (fd, &c,1);  // checksumme;
	// auf antwort warten: diese 4 byte und 4 antwortbyte:
		for (i=0;i<8;i++)
		{
			if (read(fd,&c,1)<1)
			{
				printf("\ntime out error, gespeicherte Ladung löschen\n");
				e = 1;
				break;
			}   
		}
	}
	ecm_exit(fd);
	}
	if (e)
		printf("\n\n\033[31m   Fehler beim löschen der gesp. Ladung!\033[0m\n");
	else
		printf("\n\n\033[32m   gespeicherte Ladung erfolgreich gelöscht!\033[0m\n");	

	printf ("\nZurück zum Hauptmenü mit enter-Taste!\n");
	getchar();
}

void wartungs_ladung(void)
{
//wartungsladung:
//pc: 83 F0 1 8B 
//auto:C3 F0 1 4B 
//dazwischen text, eventuell anstecken.
//pc: 83 77 90 75 
//auto: C3 77 90 35 
//danach keine kommunikation mehr!

	int fd, i, e=1;
	char c;
	clrscr();
	printf("\033[1;1f   setze Wartungsladung:\n\n");
	if (ecm_init(&fd) == 0)
	{
	e=0;
//  befehl speicherung:
	c = 0x83; write (fd, &c,1);	
	c = 0xF0; write (fd, &c,1);
	c = 1; write (fd, &c,1);
	c = 0x8b; write (fd, &c,1);  // checksumme;
// auf antwort warten: diese 4 byte und 4 antwortbyte:
	for (i=0;i<8;i++)
	{
		if (read(fd,&c,1)<1)
		{
			printf("\ntime out error, setze Wartungsladung\n");
			e = 1;
			break;
		}   
	}
	if (e==0)
	{
	//  befehl wartungsladung setzen:
		c = 0x83; write (fd, &c,1);	
		c = 0x77; write (fd, &c,1);	
		c = 0x90; write (fd, &c,1);
		c = 0x75; write (fd, &c,1);  // checksumme;
	// auf antwort warten: diese 4 byte und 4 antwortbyte:
		for (i=0;i<8;i++)
		{
			if (read(fd,&c,1)<1)
			{
				printf("\ntime out error, Wartungsladung setzen\n");
				e = 1;
				break;
			}   
		}
	}
	ecm_exit(fd);
	}
	if (e)
		printf("\n\n\033[31m   Fehler beim Setzen Wartungsladung!\033[0m\n");
	else
		printf("\n\n\033[32m   Wartungsladung erfolgreich gesetzt!\033[0m\n");	

	printf ("\nZurück zum Hauptmenü mit enter-Taste!\n");
	getchar();
}


void loesche_wasserbedarf(void)
{
//nach wartungsladung und wasserfüllen:
//löschen wasserbedarf:
//pc: 83 F0 1 8B 
//auto: C3 F0 1 4B
//pc: 83 77 81 84 
//auto: C3 77 81 44 
//ca 1 sec pause!
//pc: 82 F 6E    // beenden der kommunikation
//auto: C2 F 2E 

	int fd, i, e=1;
	char c, key;
	clrscr();
	printf("\033[1;1f   loesche Wasserbedarf:\n\n");
	printf("Nur nach einer Wartungsladung und aufgefülltem Wasser anwenden!!\n");
	printf("weiter mit j, abbruch mit n:");
	key = getchar();getchar();
	if (key == 'j')
	{
		if (ecm_init(&fd) == 0)
		{
			e=0;
		//  befehl speicherung:
			c = 0x83; write (fd, &c,1);	
			c = 0xF0; write (fd, &c,1);
			c = 1; write (fd, &c,1);
			c = 0x8b; write (fd, &c,1);  // checksumme;
		// auf antwort warten: diese 4 byte und 4 antwortbyte:
			for (i=0;i<8;i++)
			{
				if (read(fd,&c,1)<1)
				{
					printf("\ntime out error, loesche Wasserbedarf\n");
					e = 1;
					break;
				}   
			}
			if (e==0)
			{
			//  befehl wartungsladung setzen:83 77 81 84
				c = 0x83; write (fd, &c,1);	
				c = 0x77; write (fd, &c,1);	
				c = 0x81; write (fd, &c,1);
				c = 0x84; write (fd, &c,1);  // checksumme;
			// auf antwort warten: diese 4 byte und 4 antwortbyte:
				for (i=0;i<8;i++)
				{
					if (read(fd,&c,1)<1)
					{
						printf("\ntime out error, loesche Wasserbedarf\n");
						e = 1;
						break;
					}   
				}
			}
			ecm_exit(fd);
		}
		if (e)
			printf("\n\n\033[31m   Fehler beim Loeschen Wasserbedarf!\033[0m\n");
		else
			printf("\n\n\033[32m   Wasserbedarf erfolgreich geloescht!\033[0m\n");	

	}
	printf ("\nZurück zum Hauptmenü mit enter-Taste!\n");
	getchar();
}

#undef OBD_LADUNG
