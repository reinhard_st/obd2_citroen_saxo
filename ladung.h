/**************************************************************************/
/*     ODB-software für peugeot 106e                                      */
/*     (c) rst                                                            */
/*     für usb-odb mit 409.1 kkl kabel (ft232)                            */
/*     file: ladung.h                                                     */
/*           funktionen für ladung anzeigen, löschen und programmieren    */
/*                                                                        */
/*                                                                        */
/**************************************************************************/


#ifdef OBD_LADUNG
	#define EXT extern
#else
	#define EXT 
#endif

EXT void abfr_ladung(void);
// öffnet schnittstelle, fragt alle relevanten daten ab und gibt sie aus.
// schliesst schnittstelle wieder



EXT void loes_ladung(void);
// öffnet schnittstelle, Löscht die gespeicherte Ladung.
// schliesst schnittstelle wieder

EXT void wartungs_ladung(void);
// öffnet schnittstelle, setzt Wartungsladung.
// schliesst schnittstelle wieder

EXT void loesche_wasserbedarf(void);
// öffnet schnittstelle, loescht Wasserbedarf.
// schliesst schnittstelle wieder

#undef EXT
